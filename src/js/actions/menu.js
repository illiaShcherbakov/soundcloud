export function activeMenu(genre) {
  return {
    type: 'ACTIVE_MENU',
    activeGenre: genre
  }
}