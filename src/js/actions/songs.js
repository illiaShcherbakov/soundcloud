

function loader() {
  return {
    type: 'TOGGLE_LOADER'
  }
}

function clearSongs() {
  return {
    type: 'CLEAR'
  }
}

function getFistSogns(tracks, next_href) {
  return {
    type: 'GET_FIRST_SONGS',
    data: tracks,
    nextHref: next_href
  }
}

export function middlewareGetSogns(genre) {
  return dispatch => {
    dispatch(loader());
    dispatch(clearSongs());
    fetch('http://api.soundcloud.com/tracks?&client_id=f4323c6f7c0cd73d2d786a2b1cdae80c&genres='+ genre + '&limit=60&linked_partitioning=1').then(
      tracks => {
        const songs = tracks.json();
        return songs;
      }).then( songs => {
        dispatch(getFistSogns(songs.collection, songs.next_href));
        dispatch(loader());
      });
  }
}

function addSongs(newSongs, next_href) {
  return {
    type: 'ADD_SONGS',
    addedSongs: newSongs,
    nextHref: next_href
  }
}

export function middlewareAddSongs(href) {
  return dispatch => {
    dispatch(loader());
    fetch(href).then(
      tracks => {
        const songs = tracks.json();
        return songs;
      }).then( songs => {
      setTimeout(() => {
        dispatch(addSongs(songs.collection, songs.next_href));
        dispatch(loader());
      }, 1000)
    });
  }
}