import React from 'react';
import ReactDOM from 'react-dom';

import App from 'containers/App';
import Songs from 'containers/Songs';
import Test from 'containers/test';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers/index';

import {Router, Route, browserHistory, IndexRoute, hashHistory } from 'react-router';


const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

ReactDOM.render(
  <Provider store={store}>
      <Router history={hashHistory}>
        <Route path="/" component={App}>
          <IndexRoute component={Songs}/>
          <Route path="/songs/:genre" component={Songs}/>
        </Route>
        <Route path="/test" component={Test}/>
      </Router>
  </Provider>, document.getElementById("app"));




