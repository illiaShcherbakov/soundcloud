import { combineReducers } from 'redux';
import { songs } from './songs';
import { menu } from './menu';


const rootReducer = combineReducers({
  songs,
  menu
});

export default rootReducer;