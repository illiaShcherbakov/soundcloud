const initialState = {
  activeMenuItem: 'Chill'
};

export const menu = (state = initialState, action) => {
  switch (action.type) {
    case 'ACTIVE_MENU':
      return Object.assign({}, state, {
        activeMenuItem: action.activeGenre
      });
  }
  return state;
};