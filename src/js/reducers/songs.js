const initialState = {
  songs: null,
  nextHref: null,
  loader: false
};

export const songs = (state = initialState, action) => {
  switch(action.type) {
    case 'GET_FIRST_SONGS':
      return Object.assign({}, state, {
        songs: action.data,
        nextHref: action.nextHref
      });
    case 'ADD_SONGS':
      const newSongsArray = state.songs.concat(action.addedSongs);
      return Object.assign({}, state, {
        songs: newSongsArray,
        nextHref: action.nextHref
      });
    case 'TOGGLE_LOADER':
      let newLoader;
      state.loader ? newLoader = false : newLoader = true;
      return Object.assign({}, state, {
        loader: newLoader
      });
    case 'CLEAR':
      return Object.assign({}, state, {
        songs: null
      });
  }
  return state;
};