import React from 'react';

const Loader = (props) => {

  let classes = 'cs-loader';
  props.isVisible ? classes += ' active' : classes;

  return (
    <div className={classes}>
      <div className="cs-loader-inner">
        <label>	●</label>
        <label>	●</label>
        <label>	●</label>
        <label>	●</label>
        <label>	●</label>
        <label>	●</label>
      </div>
    </div>
  )
};

export default Loader;