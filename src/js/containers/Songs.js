import React from 'react';
import { connect } from 'react-redux';

import Song from './Song';
import Player from './Player';

import { middlewareGetSogns} from "../actions/songs";
import { activeMenu } from '../actions/menu';

class Songs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      streamSongId: null,
      showSongs: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if ( this.props.params.genre !== nextProps.params.genre ) {
      this.props.middlewareGetSogns(nextProps.params.genre);
    }
  }

  getStreamSongId(id) {
    this.setState({
      streamSongId: id
    });
  }

  componentWillMount() {
    const genre = this.props.params.genre == undefined ? this.props.firstLoadGenre : this.props.params.genre;
    this.props.middlewareGetSogns(genre);
    this.props.activeMenu(genre);
  }

  render() {
    var songList = [];
    if ( this.props.songs !== null ) {
       songList = this.props.songs.map( item => {

        return ( item == undefined ? (
            ''
            ):(
              <Song
                key={item.id}
                id={item.id}
                artwork={item.artwork_url}
                title={item.title}
                streamUrl={item.stream_url}
                sendStreamSongId={(e) => this.getStreamSongId(e)}
              />
            )
        )
      });
    }

    let classes = 'songs';
    this.state.showSongs ? classes += ' active' : classes;

    return(
      <div className={classes}>
        {songList}
        { this.state.streamSongId !== null ? (
            <Player songsData={this.props.songs} streamSongId={this.state.streamSongId} />
          ) : (
            ''
          )}
      </div>

    )
  }
}

const mapStateToProps = (state) => {
  //console.log(state.songs.songs);
  return {
    songs: state.songs.songs,
    firstLoadGenre: state.menu.activeMenuItem
  }
};

export default connect(mapStateToProps, {middlewareGetSogns, activeMenu})(Songs);