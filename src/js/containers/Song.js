import React from 'react';

export default class Song extends React.Component {

  constructor(props) {
    super(props);
  }

  handleClick(e) {
    this.props.sendStreamSongId(this.props.id);
  }


  render() {

    const [author, title] = this.props.title.split(' - ');

    return (
      <div className="song">
        <img className="" src={this.props.artwork == null ? './img/no_img.jpg' : this.props.artwork} alt="" onClick={(e) => this.handleClick(e)}/>

        <div className="metadata">
          <p className="author">{title ? author : ''}</p>
          <p className="title">{title ? title : author }</p>
        </div>
        
      </div>
    )
  }
};
