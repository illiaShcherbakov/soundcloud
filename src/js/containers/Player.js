import React from 'react';

export default class Player extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      playlist: null,
      isVisible: false,
      songData: {},
      prevDisable: false,
      nextDisable: false
    }
  }

  componentWillMount() {

    var number, songToPlay;

    //setting the playlist
    this.setState({
      playlist: this.props.songsData
    });

    for (let i = 0; i < this.props.songsData.length; i++ ) {
      if ( this.props.songsData[i].id == this.props.streamSongId ) {
        this.setState({
          isVisible: true,
          songData: {
            number: i,
            songToPlay: this.props.songsData[i]
          }
        });
      }
    }
  }

  componentWillReceiveProps(nextProps) {

    if ( nextProps.songsData != null ) {

      if ( this.props.streamSongId == nextProps.streamSongId ) {
        this.setState({
          playlist: nextProps.songsData
        }, () => {
          for (let i = 0; i < this.state.playlist.length; i++ ) {
            this.setState({
              isVisible: true,
              songData: {
                number: 0,
                songToPlay: this.state.playlist[0]
              },
              prevDisable: false,
              nextDisable: false
            });
          }
          document.getElementById('player').autoplay = false;
        });

      } else {
        this.setState({
          playlist: nextProps.songsData
        }, () => {
          for (let i = 0; i < this.state.playlist.length; i++ ) {
            if ( this.state.playlist[i].id == nextProps.streamSongId ) {
              this.setState({
                isVisible: true,
                songData: {
                  number: i,
                  songToPlay: this.state.playlist[i]
                },
                prevDisable: false,
                nextDisable: false
              });
            }
          }
        });
        document.getElementById('player').autoplay = true;
        document.getElementById('player').pause();
      }
    }
  }

  componentDidMount() {
    const player = document.getElementById('player');
    player.volume = 0.2;
    player.autoplay = true;
  }

  handlePrev(e) {
    if ( this.state.songData.number - 1 == 0 ) {
      this.setState({
        songData: {
          number: this.state.songData.number - 1,
          songToPlay: this.state.playlist[this.state.songData.number - 1]
        },
        prevDisable: true
      })
    } else {
      this.setState({
        songData: {
          number: this.state.songData.number - 1,
          songToPlay: this.state.playlist[this.state.songData.number - 1]
        }
      })
    }
    document.getElementById('player').autoplay = true;
  }

  handleNext(e) {
    if ( this.state.songData.number + 1 == 0 ) {
      this.setState({
        songData: {
          number: this.state.songData.number + 1,
          songToPlay: this.state.playlist[this.state.songData.number - 1]
        },
        nextDisable: true
      })
    } else {
      this.setState({
        songData: {
          number: this.state.songData.number + 1,
          songToPlay: this.state.playlist[this.state.songData.number + 1]
        }
      })
    }
    document.getElementById('player').autoplay = true;
  }

  handleEnd() {
    this.handleNext();
  }

  render(){



    let classes = 'player';
    this.state.isVisible ? classes += ' active' : classes;

    const [author, title] = this.state.songData.songToPlay.title.split(' - ');
    const url = this.state.songData.songToPlay.stream_url + '?&client_id=f4323c6f7c0cd73d2d786a2b1cdae80c';
    //&#9194; &#x23e9;
    return (
      <div className={classes}>
        <div className="player-meta">
          <span className="palyer-author">{title ? author : ''}</span>
          {title ? (
              <span className="palyer-author"> - </span>
            ):(
              ''
            )}
          <span className="player-title">{title ? title : author}</span>
        </div>
        <button className="player-prev" onClick={(e) => this.handlePrev(e)} disabled={this.state.prevDisable}></button>
        <audio className="player-audio" id="player" src={url} controls onEnded={() => this.handleEnd()}></audio>
        <button className="player-next" onClick={(e) => this.handleNext(e)} disabled={this.state.nextDisable}></button>
      </div>

    )
  }
}