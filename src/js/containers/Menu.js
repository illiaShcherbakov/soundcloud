import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import MenuItem from './MenuItem';

import { middlewareGetSogns } from "../actions/songs";

class Menu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listOfGenres: [
        {name:'Chill', isActive: true},
        {name:'Deep', isActive: false},
        {name:'Dubstep', isActive: false},
        {name:'House', isActive: false},
        {name:'Progressive', isActive: false},
        {name:'Tech', isActive: false},
        {name:'Trance', isActive: false},
        {name:'Tropical', isActive: false}
      ]
    }
  }

  componentWillReceiveProps(nextProps) {
    this.toggleClass(nextProps.activeGenre);
  }

  getSongs(genre) {
    this.props.middlewareGetSogns(genre);
  }

  toggleClass(name) {
    const newList = this.state.listOfGenres.map( item => {
      if ( item.isActive == true ) {
        item.isActive = false;
      }
      if ( item.name === name ) {
        item.isActive = true;
      }
      return item;
    });

    this.setState({
      listOfGenres: newList
    })
  }


  render() {
    const muneItems = this.state.listOfGenres.map( item => {
      return (
       <MenuItem
         key={item.name.toString()}
        getSongs={(e) => this.getSongs(e)}
        name={item.name}
        isActive={item.isActive}
         toggleClass={(e)=> this.toggleClass(e)}
       />
      )
    });


    return (
      <ul className="menu">
        {muneItems}
      </ul>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    activeGenre: state.menu.activeMenuItem,
  }
};


export default connect(mapStateToProps, {middlewareGetSogns})(Menu);