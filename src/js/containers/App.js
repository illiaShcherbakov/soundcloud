import React from 'react';
import Menu from './Menu';
import Loader from '../components/Loader';

import { connect } from 'react-redux';

import { middlewareAddSongs } from '../actions/songs';


class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', () => {
      if (document.body.scrollTop + window.innerHeight >= document.body.scrollHeight) {
        db_moar(this.props.nextHref);
        document.body.scrollTop = document.body.scrollTop + 70;
      }
    });

    var db_moar = debounce((nextHref) => this.props.middlewareAddSongs(nextHref), 700, true);

    function debounce(func, wait, immediate) {
      var timeout;
      return function() {
        var context = this, args = arguments;
        var later = function() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    }
  }

  render() {
    return(
      <div>
        <Menu />
        { this.props.children }
        <Loader isVisible={this.props.loader} />
      </div>

    )
  }
}

const mapStateToProps = (state) => {

  return {
    nextHref: state.songs.nextHref,
    loader: state.songs.loader
  }
};

export default connect(mapStateToProps, {middlewareAddSongs})(App);