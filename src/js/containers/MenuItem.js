import React from 'react';
import { Link } from 'react-router';

export default class MenuItem extends React.Component {

  constructor(props) {
    super(props);
  }

  handleClick(e) {
    this.props.toggleClass(this.props.name);
  }

  render() {
    return  this.props.isActive ? (
        <li className="active">
          <Link activeClassName="active" to={'/songs/' + this.props.name} onClick={(e) => this.handleClick(e)}>{this.props.name}</Link>
        </li>
      ):(
      <li>
        <Link to={'/songs/' + this.props.name} onClick={(e) => this.handleClick(e)}>{this.props.name}</Link>
      </li>
      )
  }
};
